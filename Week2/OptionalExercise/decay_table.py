"""
Given a brachytherapy radioactive source,
generate a source decay table that displays the activity bi-daily
(e.g., at 6 AM and 6 PM) everyday for 10 days.
"""
