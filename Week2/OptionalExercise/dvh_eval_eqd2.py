"""
Given a dictionary of structures and their DVH values (in physical dose),
we want to evaluate whether they satisfy their constraints (in EQD2) for plan evaluation
"""

# Example data for a 36.25 Gy in 5 fractions prostate plan
dvh_data = {
    "Bladder": {"DMax": 38, "DMean": 30},
    "Rectum": {"DMax": 40, "DMean": 20},
    "FemoralHead_L": {"DMax": 21, "DMean": 7},
    "FemoralHead_R": {"DMax": 29, "DMean": 8},
    "PenileBulb": {"DMax": 33, "DMean": 24},
    "Cauda": {"DMax": 30, "DMean": 15},
}
# Example constraints in EQD2
eqd2_constraints = {
    "Bladder": {"DMax": "<=90", "DMean": "<=65"},
    "Rectum": {"DMax": "<=75", "DMean": "<=50"},
    "PenileBulb": {"DMax": "<=50", "DMean": "<=35"},
    "Cauda": {"DMax": "<=50", "DMean": "<=40"},
}
