"""
Given a dictionary of structures and their DVH values,
we want to evaluate whether they satisfy their constraints for plan evaluation
"""

# Example data
dvh_data = {
    "Heart": {"V20": 15, "V5": 30},
    "Lung_L": {"V20": 20, "V10": 65},
    "Lung_R": {"V20": 0, "V10": 5}
}
constraints = {
    "Heart": {"V20": "<=5", "V5": "<=35"},
    "Lung_L": {"V20": "<=20", "V10": "<=40"},
    "Lung_R": {"V20": "<=20", "V10": "<=40"}
}

# Loop through each structure
for structure in dvh_data.keys():
    # Loop through each DVH metric
    for metric in dvh_data[structure].keys():
        # Get the constraint for the structure and metric
        constraint = constraints[structure][metric]
        # Get the DVH value for the structure and metric
        dvh_value = dvh_data[structure][metric]
        # Evaluate the constraint
        if eval(str(dvh_value) + constraint):
            print(structure + " " + metric + " constraint passed")
        else:
            print(structure + " " + metric + " constraint failed")
