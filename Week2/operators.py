# Operators

# Arithmetic Operators
# +, -, *, /, %, **, //

# Addition
print(2 + 2)

# Subtraction
print(2 - 2)

# Multiplication
print(2 * 2)

# Division
print(2 / 2)

# Modulus
print(3 % 2)

# Exponentiation
print(2 ** 3)

# Floor Division
print(3 // 2)

# Comparison Operators
# ==, !=, >, <, >=, <=

# Equal
print(2 == 2)

# Not Equal
print(2 != 2)

# Greater Than
print(2 > 2)

# Less Than
print(2 < 2)

# Greater Than or Equal To
print(2 >= 2)

# Less Than or Equal To
print(2 <= 2)

# Logical Operators
# and, or, not

# and
print(True and True)

# or
print(True or False)

# not
print(not True)

# Bitwise Operators
# &, |, ^, ~, <<, >>

# And
print(5 & 4)
# why?
print(bin(5) + " & " + bin(4) + " = " + bin(5 & 4))

# Or
print(5 | 4)
print(bin(5) + " | " + bin(4) + " = " + bin(5 | 4))

# Xor
print(5 ^ 4)
print(bin(5) + " ^ " + bin(4) + " = " + bin(5 ^ 4))

# Conditional Operators
# if, elif, else

#Example

if 2 > 1:
    print("2 is greater than 1")
elif 2 < 1:
    print("2 is less than 1")
else:
    print("2 is equal to 1")
