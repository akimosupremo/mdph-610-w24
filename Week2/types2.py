# Python types

# Integers
x = 4

# Floats
y = 1.0

# Strings
z = "Hello World"

# Booleans
a = True

# Lists
b = [1, 2, 3, 4, 5]
print(b)
print(b[0])
b[0] = 10
print(b)

# Tuples
# Tuples are immutable
c = (1, 2, 3, 4, 5)
print(c)
print(c[0])
# c[0] = 10

# Dictionaries
# Dictionaries are key-value pairs
d = {"name": "John", "age": 30}

# Sets
# Sets are unordered collections of unique elements
# Removes duplicates
e = {1, 2, 3, 4, 5, 5, 5}
print(e)

print(x+a)
print(x*z)
