// Calculate BED in radiotherapy
function calculateBED() {
    var dosePerFraction = document.getElementById("dosePerFraction").value;
    var fractions = document.getElementById("fractions").value;
    var alphaBetaRatio = document.getElementById("alphaBeta").value;
    var BED = dosePerFraction * fractions * (1 + dosePerFraction / alphaBetaRatio);
    makeAlert("The BED is " + BED);
}

// Calculate EQD2 in radiotherapy
function calculateEQD2() {
    var dosePerFraction = document.getElementById("dosePerFraction").value;
    var fractions = document.getElementById("fractions").value;
    var alphaBetaRatio = document.getElementById("alphaBeta").value;
    var EQD2 = dosePerFraction * fractions * ((dosePerFraction + dosePerFraction / alphaBetaRatio) / (2 + alphaBetaRatio));
    makeAlert("The EQD2 is " + EQD2);
}

// Alert function
function makeAlert(text) {
    alert(text);
}
