# Import necessary modules from flask
from flask import Flask, render_template, request, jsonify

# Initialize a new Flask web application
app = Flask(__name__)

# Define the route for the home page
@app.route('/')
def welcome():
    # Render the welcome.html template
    return render_template('welcome.html')

# Define the route for the calculation page
@app.route('/calculate')
def calculate():
    # Render the calculation.html template
    return render_template('calculation.html')

# Define the route for the calculation API, accepting POST requests
@app.route('/calculate', methods=['POST'])
def calculate_bed_eqd2():
    # Get the JSON data sent in the request
    data = request.get_json()
    # Extract the necessary values from the data
    dose_per_fraction = data['dosePerFraction']
    total_fractions = data['totalFractions']
    alpha_beta = data['alphaBeta']

    # Calculate the Biological Effective Dose (BED)
    bed = dose_per_fraction * total_fractions * (1 + (dose_per_fraction / alpha_beta))
    # Calculate the Equivalent Dose in 2 Gy fractions (EQD2)
    eqd2 = bed / ((2 / alpha_beta) + 1)

    # Return the results as JSON
    return jsonify(bed=bed, eqd2=eqd2)

# If this script is run directly, start the Flask web server
if __name__ == '__main__':
    app.run(debug=True)
