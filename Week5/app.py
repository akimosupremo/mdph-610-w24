# Import necessary modules from flask
from flask import Flask, render_template, request, jsonify

# Initialize a new Flask web application
app = Flask(__name__)

# Create a treament model class
class Treatment:
    def __init__(self, dose_per_fraction, total_fractions, alpha_beta):
        self.dose_per_fraction = dose_per_fraction
        self.total_fractions = total_fractions
        self.alpha_beta = alpha_beta

    def calculate_bed(self):
        return self.dose_per_fraction * self.total_fractions * (1 + (self.dose_per_fraction / self.alpha_beta))

    def calculate_eqd2(self):
        return self.calculate_bed() / ((2 / self.alpha_beta) + 1)

    def to_dict(self):
        return {
            'dose_per_fraction': self.dose_per_fraction,
            'total_fractions': self.total_fractions,
            'alpha_beta': self.alpha_beta,
            'bed': self.calculate_bed(),
            'eqd2': self.calculate_eqd2()
        }

# Define the route for the home page
@app.route('/')
def welcome():

    name = 'Ackeem'

    # Render the welcome.html template
    return render_template('welcome.html', name=name)

# Define the route for the calculation page with both GET and POST requests
@app.route('/calculate', methods=['GET', 'POST'])
def calculate():

    # If the request is a POST request
    if request.method == 'POST':
        # Extract the necessary values from the form
        dose_per_fraction = float(request.form['dosePerFraction'])
        total_fractions = int(request.form['totalFractions'])
        alpha_beta = int(request.form['alphaBeta'])

        # Create a new Treatment object
        treatment = Treatment(dose_per_fraction, total_fractions, alpha_beta)

        # Calculate the BED and EQD2
        bed = treatment.calculate_bed()
        eqd2 = treatment.calculate_eqd2()

        # Return the results
        return render_template('calculation.html', results=treatment.to_dict())

    # Render the calculation.html template
    return render_template('calculation.html', results=None)



# If this script is run directly, start the Flask web server
if __name__ == '__main__':
    app.run(debug=True)
