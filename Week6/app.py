# Import necessary modules from flask
from flask import Flask, render_template, request, jsonify
import models

db_config = {
  'user': 'root',
  'password': 'root',
  'host': '127.0.0.1',
  'port': 8889,
  'database': 'FlaskDemoW24',
  'raise_on_warnings': True
}

db = models.Database(db_config)

# Initialize a new Flask web application
app = Flask(__name__)

# Define the route for the home page
@app.route('/')
def welcome():

    name = 'Ackeem'

    # Render the welcome.html template
    return render_template('welcome.html', name=name)

# Define the route for the calculation page with both GET and POST requests
@app.route('/calculate', methods=['GET', 'POST'])
def calculate():

    structures = models.Structure.get_all_structures(db)

    # loop through the structures and assign structure metrics to each structure
    for structure in structures:
        structure.metrics = models.StructureMetric.get_all_structure_metrics(db, structure.id)

    dvh_metrics = models.DVHMetric.get_all_dvh_metrics(db)

    # Render the calculation.html template
    return render_template('calculation.html', structures=structures, dvh_metrics=dvh_metrics)

# Method for adding dvh metric to the database on sumission given structure id in url
@app.route('/add_dvh_metric/<int:structure_id>', methods=['POST'])
def add_dvh_metric(structure_id):

    # get dvh metric id, metric value and alpha beta from the form
    dvh_metric_id = request.form['dvh_metric']
    metric_value = request.form['metric_value']
    alpha_beta = request.form['alpha_beta']

    structure = models.Structure(db, structure_id)
    dvh_metric = models.DVHMetric(db, dvh_metric_id)

    # create a new StructureMetric object
    structure_metric = models.StructureMetric(db, structure, dvh_metric, metric_value, alpha_beta)

    # save the new StructureMetric to the database
    structure_metric.save()

    # redirect to the calculation page
    return calculate()

# Method for deleting dvh metric from the database on sumission given structure id and metric id in url
@app.route('/delete_dvh_metric/<int:structure_id>/<int:dvh_metric_id>')
def delete_dvh_metric(structure_id, dvh_metric_id):

        structure = models.Structure(db, structure_id)
        dvh_metric = models.DVHMetric(db, dvh_metric_id)

        # create a new StructureMetric object
        structure_metric = models.StructureMetric(db, structure, dvh_metric)

        # delete the StructureMetric from the database
        structure_metric.delete()

        # redirect to the calculation page
        return calculate()

# Method for calculating BED and EQD2
@app.route('/calculate_dose')
def calculate_dose():
    structures = models.Structure.get_all_structures(db)

    # loop through the structures and assign structure metrics to each structure
    for structure in structures:
        metrics = models.StructureMetric.get_all_structure_metrics(db, structure.id)
        for metric in metrics:
            metric.calculate_bed_eqd2(30)


    # redirect to the calculation page
    return calculate()

# If this script is run directly, start the Flask web server
if __name__ == '__main__':
    app.run(debug=True)
