# Import necessary modules from flask
from flask import Flask, render_template, request, jsonify
import models
import os
from pydicom import dcmread
from dicompylercore import dicomparser, dvhcalc
import matplotlib
matplotlib.use('Agg')  # Ensure matplotlib does not use any GUI backend.
import numpy as np
import matplotlib.pyplot as plt
import io
import base64

db_config = {
  'user': 'root',
  'password': 'root',
  'host': '127.0.0.1',
  'port': 8889,
  'database': 'FlaskDemoW24',
  'raise_on_warnings': True
}

db = models.Database(db_config)

# Initialize a new Flask web application
app = Flask(__name__)

# Create a treament model class
class Treatment:
    def __init__(self, dose_per_fraction, total_fractions, alpha_beta):
        self.dose_per_fraction = dose_per_fraction
        self.total_fractions = total_fractions
        self.alpha_beta = alpha_beta

    def calculate_bed(self):
        return self.dose_per_fraction * self.total_fractions * (1 + (self.dose_per_fraction / self.alpha_beta))

    def calculate_eqd2(self):
        return self.calculate_bed() / ((2 / self.alpha_beta) + 1)

    def to_dict(self):
        return {
            'dose_per_fraction': self.dose_per_fraction,
            'total_fractions': self.total_fractions,
            'alpha_beta': self.alpha_beta,
            'bed': self.calculate_bed(),
            'eqd2': self.calculate_eqd2()
        }

APP_ROOT = os.path.dirname(os.path.abspath(__file__))

RD_FILE_PATH = os.path.join(APP_ROOT, 'static/dicom/RD/rt_dose.dcm')
RTSTRUCT_FILE_PATH = os.path.join(APP_ROOT, 'static/dicom/RS/rt_struct.dcm')

# Define the route for the home page
@app.route('/')
def welcome():

    # Read the DICOM RD file
    rd_dcm = dcmread(RD_FILE_PATH)
    rtstruct_dcm = dcmread(RTSTRUCT_FILE_PATH)

   # Parse the DICOM files
    dp = dicomparser.DicomParser(rtstruct_dcm)
    dp_dose = dicomparser.DicomParser(rd_dcm)

    structures = dp.GetStructures()

    # Get DVHs calculated from the RD file
    dvh_data = dp_dose.GetDVHs()

    # Initialize a figure for plotting
    fig, ax = plt.subplots()

    # Calculate and plot DVH for each structure
    for structure_id, structure in structures.items():
        try:
            if structure_id in dvh_data:
                dvh = dvh_data[structure_id]
                # convert to relative volume
                dvh = dvh.relative_volume
                ax.plot(dvh.bincenters , dvh.counts, label=structure['name'] or f"Structure {structure_id}", color=None if not isinstance(structure['color'], np.ndarray) else structure['color'] / 255)
        except Exception as e:
            print(f"Error calculating DVH for structure {structure_id}: {e}")

    ax.set_title('DVH for all structures')
    ax.set_xlabel('Dose (Gy)')
    ax.set_ylabel('Volume (%)')
    ax.legend(loc='best')

    # Convert plot to a PNG image encoded in base64 for embedding in HTML
    img = io.BytesIO()
    plt.savefig(img, format='png', dpi=300, bbox_inches='tight')
    plt.close(fig)  # Close the figure to free memory
    img.seek(0)
    plot_url = base64.b64encode(img.getvalue()).decode('utf-8')

    return render_template('welcome.html', plot_url=plot_url)

# Define the route for the calculation page with both GET and POST requests
@app.route('/calculate', methods=['GET', 'POST'])
def calculate():

    # get all structures from the database
    structures = models.Structure(db).all(db)
    print(structures)

    # loop through the structures and assign structure metrics to each structure
    for structure in structures:
        structure['metrics'] = models.StructureMetric(db).all(db, structure['id'])

    dvh_metrics = models.DVHMetric(db).all(db)

    print(dvh_metrics)
    print(structures)

    # Render the calculation.html template
    return render_template('calculation.html', structures=structures, dvh_metrics=dvh_metrics)



# If this script is run directly, start the Flask web server
if __name__ == '__main__':
    app.run(debug=True)
