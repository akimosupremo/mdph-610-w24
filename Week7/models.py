# Importing the necessary modules
import mysql.connector
from mysql.connector import Error

# Defining the Database class
class Database:
    # Initializing the Database class with a configuration
    def __init__(self, config):
        self.config = config

    # Method to connect to the database
    def connect(self):
        try:
            # Attempt to establish a connection
            self.connection = mysql.connector.connect(**self.config)
            # Return the connection object
            return self.connection
        except Error as e:
            # Print the error if a connection cannot be established
            print(f"Error connecting to MySQL Database: {e}")
            return None

    # Method to execute a query
    def execute_query(self, query, params=None):
        result = None
        try:
            # Establish a connection
            connection = self.connect()
            if connection is not None:
                # Create a cursor object
                cursor = connection.cursor(dictionary=True)
                # If parameters are provided, execute the query with the parameters
                if params is not None:
                    cursor.execute(query, params)
                else:
                    # If no parameters are provided, execute the query without parameters
                    cursor.execute(query)
                # Fetch all the results
                result = cursor.fetchall()
                # Commit the transaction
                connection.commit()
                # Close the cursor
                cursor.close()

            # Return the result
            return result
        except Error as e:
            # Print the error if the query cannot be executed
            print(f"Error executing query: {e}")
            return None

# Create Structure
# Structure has attributes: db, id, name, type
class Structure:
    def __init__(self, db, id=None, name=None, type=None):
        self.db = db
        self.id = id
        self.name = name
        self.type = type

    # Method to save a new structure to the database
    def save(self):
        # Define the query
        query = "INSERT INTO structure (name, type) VALUES (%s, %s)"
        # Define the parameters
        params = (self.name, self.type)
        # Execute the query
        self.db.execute_query(query, params)

    # Method to update an existing structure in the database
    def update(self):
        # Define the query
        query = "UPDATE structure SET name = %s, type = %s WHERE id = %s"
        # Define the parameters
        params = (self.name, self.type, self.id)
        # Execute the query
        self.db.execute_query(query, params)

    # Method to delete a structure from the database
    def delete(self):
        # Define the query
        query = "DELETE FROM structure WHERE id = %s"
        # Define the parameters
        params = (self.id,)
        # Execute the query
        self.db.execute_query(query, params)

    # Method to retrieve all structures from the database
    @staticmethod
    def all(db):
        # Define the query
        query = "SELECT * FROM structure"
        # Execute the query
        return db.execute_query(query)

# Create DVHMetric class
# DVHMetric has attributes: db, id, label
class DVHMetric:
    def __init__(self, db, id=None, label=None):
        self.db = db
        self.id = id
        self.label = label

    # Method to save a new DVH metric to the database
    def save(self):
        # Define the query
        query = "INSERT INTO dvh_metric (label) VALUES (%s)"
        # Define the parameters
        params = (self.label,)
        # Execute the query
        self.db.execute_query(query, params)

    # Method to update an existing DVH metric in the database
    def update(self):
        # Define the query
        query = "UPDATE dvh_metric SET label = %s WHERE id = %s"
        # Define the parameters
        params = (self.label, self.id)
        # Execute the query
        self.db.execute_query(query, params)

    # Method to delete a DVH metric from the database
    def delete(self):
        # Define the query
        query = "DELETE FROM dvh_metric WHERE id = %s"
        # Define the parameters
        params = (self.id,)
        # Execute the query
        self.db.execute_query(query, params)

    # Method to retrieve all DVH metrics from the database
    @staticmethod
    def all(db):
        # Define the query
        query = "SELECT * FROM dvh_metric"
        # Execute the query
        return db.execute_query(query)

# Create StructureMetric class which is a join table between Structure and DVHMetric
# StructureMetric has attributes: db, structure class, dvh_metric class, metric_value, alpha_beta
class StructureMetric:
    def __init__(self, db, structure=None, dvh_metric=None, metric_value=None, alpha_beta=None):
        self.db = db
        self.structure = structure
        self.dvh_metric = dvh_metric
        self.metric_value = metric_value
        self.alpha_beta = alpha_beta


    # Method to save a new structure metric to the database
    def save(self):
        # Define the query
        query = "INSERT INTO structure_metric (structure_id, dvh_metric_id, metric_value, alpha_beta) VALUES (%s, %s, %s, %s)"
        # Define the parameters
        params = (self.structure.id, self.dvh_metric.id, self.metric_value, self.alpha_beta)
        # Execute the query
        self.db.execute_query(query, params)

    # Method to update an existing structure metric in the database
    def update(self):
        # Define the query
        query = "UPDATE structure_metric SET structure_id = %s, dvh_metric_id = %s, metric_value = %s, alpha_beta = %s WHERE id = %s"
        # Define the parameters
        params = (self.structure.id, self.dvh_metric.id, self.metric_value, self.alpha_beta, self.id)
        # Execute the query
        self.db.execute_query(query, params)

    # Method to delete a structure metric from the database
    def delete(self):
        # Define the query
        query = "DELETE FROM structure_metric WHERE id = %s"
        # Define the parameters
        params = (self.id,)
        # Execute the query
        self.db.execute_query(query, params)

    # Method to retrieve all structure metrics given a structure_id
    @staticmethod
    def all(db, structure_id):
        # Define the query
        query = "SELECT sm.*, dm.label FROM structure_metric sm JOIN dvh_metric dm ON sm.dvh_metric_id = dm.id WHERE sm.structure_id = %s"
        # Define the parameters
        params = (structure_id,)
        # Execute the query
        return db.execute_query(query, params)
