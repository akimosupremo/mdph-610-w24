# Importing the necessary modules
import mysql.connector
from mysql.connector import Error

# Defining the Database class
class Database:
    # Initializing the Database class with a configuration
    def __init__(self, config):
        self.config = config

    # Method to connect to the database
    def connect(self):
        try:
            # Attempt to establish a connection
            self.connection = mysql.connector.connect(**self.config)
            # Return the connection object
            return self.connection
        except Error as e:
            # Print the error if a connection cannot be established
            print(f"Error connecting to MySQL Database: {e}")
            return None

    # Method to execute a query
    def execute_query(self, query, params=None):
        result = None
        try:
            # Establish a connection
            connection = self.connect()
            if connection is not None:
                # Create a cursor object
                cursor = connection.cursor(dictionary=True)
                # If parameters are provided, execute the query with the parameters
                if params is not None:
                    cursor.execute(query, params)
                else:
                    # If no parameters are provided, execute the query without parameters
                    cursor.execute(query)
                # Fetch all the results
                result = cursor.fetchall()
                # Commit the transaction
                connection.commit()
                # Close the cursor
                cursor.close()

            # Return the result
            return result
        except Error as e:
            # Print the error if the query cannot be executed
            print(f"Error executing query: {e}")
            return None

# Create Structure
# Structure has attributes: db, id, name, type
class Structure:
    def __init__(self, db, id=None, name=None, type=None):
        self.db = db
        self.id = id
        self.name = name
        self.type = type

    # Method to save a new structure to the database
    def save(self):
        if (self.id is None):
            # Define the query
            query = "INSERT INTO structure (name, type) VALUES (%s, %s)"
            # Define the parameters
            params = (self.name, self.type)
            # Execute the query
            self.db.execute_query(query, params)
        else:
            query = "UPDATE structure SET name = %s, type = %s WHERE id = %s"
            params = (self.name, self.type, self.id)
            self.db.execute_query(query, params)

    # Method to delete a structure from the database
    def delete(self):
        # Define the query
        query = "DELETE FROM structure WHERE id = %s"
        # Define the parameters
        params = (self.id,)
        # Execute the query
        self.db.execute_query(query, params)

    # Method to retrieve all structures from the database
    @staticmethod
    def get_all_structures(db):
        # Define the query
        query = "SELECT * FROM structure"
        result = db.execute_query(query)
        return [Structure(db, row['id'], row['name'], row['type']) for row in result]

# Create DVHMetric class
# DVHMetric has attributes: db, id, label
class DVHMetric:
    def __init__(self, db, id=None, label=None):
        self.db = db
        self.id = id
        self.label = label

    # Method to save a new DVH metric to the database
    def save(self):
        if (self.id is None):
            # Define the query
            query = "INSERT INTO dvh_metric (label) VALUES (%s)"
            # Define the parameters
            params = (self.label,)
            # Execute the query
            self.db.execute_query(query, params)
        else:
            query = "UPDATE dvh_metric SET label = %s WHERE id = %s"
            params = (self.label, self.id)
            self.db.execute_query(query, params)

    # Method to delete a DVH metric from the database
    def delete(self):
        # Define the query
        query = "DELETE FROM dvh_metric WHERE id = %s"
        # Define the parameters
        params = (self.id,)
        # Execute the query
        self.db.execute_query(query, params)

    # Method to retrieve all DVH metrics from the database
    @staticmethod
    def get_all_dvh_metrics(db):
        # Define the query
        query = "SELECT * FROM dvh_metric"
        result = db.execute_query(query)
        return [DVHMetric(db, row['id'], row['label']) for row in result]

# Create StructureMetric class which is a join table between Structure and DVHMetric
# StructureMetric has attributes: db, structure class, dvh_metric class, metric_value, alpha_beta, bed, eqd2
class StructureMetric:
    def __init__(self, db, structure=None, dvh_metric=None, metric_value=None, alpha_beta=None, bed=None, eqd2=None):
        self.db = db
        self.structure = structure
        self.dvh_metric = dvh_metric
        self.metric_value = metric_value
        self.alpha_beta = alpha_beta
        self.bed = bed
        self.eqd2 = eqd2

    # Method to check if a structure metric already exists in the database
    def exists(self):
        # Define the query
        query = "SELECT * FROM structure_metric WHERE structure_id = %s AND dvh_metric_id = %s"
        # Define the parameters
        params = (self.structure.id, self.dvh_metric.id)
        # Execute the query
        result = self.db.execute_query(query, params)
        # Return True if the structure metric exists, otherwise return False
        return len(result) > 0

    # Method to save a new structure metric to the database
    def save(self):
        if not self.exists():
            # Define the query
            query = "INSERT INTO structure_metric (structure_id, dvh_metric_id, metric_value, alpha_beta) VALUES (%s, %s, %s, %s)"
            # Define the parameters
            params = (self.structure.id, self.dvh_metric.id, self.metric_value, self.alpha_beta)
            # Execute the query
            self.db.execute_query(query, params)
        else:
            query = "UPDATE structure_metric SET metric_value = %s, alpha_beta = %s WHERE structure_id = %s AND dvh_metric_id = %s"
            params = (self.metric_value, self.alpha_beta, self.structure.id, self.dvh_metric.id)
            self.db.execute_query(query, params)

    # Method to delete a structure metric from the database
    def delete(self):
        # Define the query
        query = "DELETE FROM structure_metric WHERE structure_id = %s AND dvh_metric_id = %s"
        # Define the parameters
        params = (self.structure.id, self.dvh_metric.id)
        # Execute the query
        self.db.execute_query(query, params)

    # Method to calcaulte BED and EQD2, given number of fractions in argument, save to database
    def calculate_bed_eqd2(self, fractions):
        # Calculate BED
        self.bed = self.metric_value * (1 + self.metric_value / self.alpha_beta / fractions)
        # Calculate EQD2
        self.eqd2 = self.bed / ((2 / self.alpha_beta) + 1)
        # Define the query
        query = "UPDATE structure_metric SET bed = %s, eqd2 = %s WHERE structure_id = %s AND dvh_metric_id = %s"
        # Define the parameters
        params = (self.bed, self.eqd2, self.structure.id, self.dvh_metric.id)
        # Execute the query
        self.db.execute_query(query, params)


    # Method to retrieve all structure metrics given a structure_id
    @staticmethod
    def get_all_structure_metrics(db, structure_id):
        # Define the query
        query = "SELECT sm.*, dm.label FROM structure_metric sm JOIN dvh_metric dm ON sm.dvh_metric_id = dm.id WHERE structure_id = %s"
        # Define the parameters
        params = (structure_id,)
        result = db.execute_query(query, params)
        return [StructureMetric(db, Structure(db, structure_id), DVHMetric(db, row['dvh_metric_id'], row['label']), row['metric_value'], row['alpha_beta'], row['bed'], row['eqd2']) for row in result]
