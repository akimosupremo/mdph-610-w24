-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Feb 08, 2024 at 03:01 AM
-- Server version: 5.7.39
-- PHP Version: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `FlaskDemoW24`
--

-- --------------------------------------------------------

--
-- Table structure for table `dvh_metric`
--

CREATE TABLE `dvh_metric` (
  `id` int(11) NOT NULL,
  `label` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dvh_metric`
--

INSERT INTO `dvh_metric` (`id`, `label`) VALUES
(1, 'DMax'),
(2, 'DMean'),
(3, 'D50'),
(4, 'V20');

-- --------------------------------------------------------

--
-- Table structure for table `structure`
--

CREATE TABLE `structure` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `type` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `structure`
--

INSERT INTO `structure` (`id`, `name`, `type`) VALUES
(1, 'Heart', 'ORGAN'),
(2, 'Lung_L', 'ORGAN');

-- --------------------------------------------------------

--
-- Table structure for table `structure_metric`
--

CREATE TABLE `structure_metric` (
  `structure_id` int(11) NOT NULL,
  `dvh_metric_id` int(11) NOT NULL,
  `metric_value` float NOT NULL,
  `alpha_beta` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `structure_metric`
--

INSERT INTO `structure_metric` (`structure_id`, `dvh_metric_id`, `metric_value`, `alpha_beta`) VALUES
(1, 1, 20, 4),
(2, 1, 60, 3),
(1, 2, 33, 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dvh_metric`
--
ALTER TABLE `dvh_metric`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `structure`
--
ALTER TABLE `structure`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `structure_metric`
--
ALTER TABLE `structure_metric`
  ADD KEY `structure_id` (`structure_id`),
  ADD KEY `dvh_metric_id` (`dvh_metric_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dvh_metric`
--
ALTER TABLE `dvh_metric`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `structure`
--
ALTER TABLE `structure`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `structure_metric`
--
ALTER TABLE `structure_metric`
  ADD CONSTRAINT `structure_metric_ibfk_1` FOREIGN KEY (`structure_id`) REFERENCES `structure` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `structure_metric_ibfk_2` FOREIGN KEY (`dvh_metric_id`) REFERENCES `dvh_metric` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
