// Function to get form data
function getFormData() {
    // Get the value of 'dosePerFraction' from the form, convert it to a float
    const dosePerFraction = parseFloat(document.getElementById('dosePerFraction').value);
    // Get the value of 'totalFractions' from the form, convert it to an integer
    const totalFractions = parseInt(document.getElementById('totalFractions').value);
    // Get the value of 'alphaBeta' from the form, convert it to a float
    const alphaBeta = parseFloat(document.getElementById('alphaBeta').value);
    // Return an object with the form data
    return { dosePerFraction, totalFractions, alphaBeta };
}

// Function to calculate BED and EQD2 using JavaScript
function calculateWithJS() {
    // Destructure the form data
    const { dosePerFraction, totalFractions, alphaBeta } = getFormData();
    // Calculate BED
    const bed = dosePerFraction * totalFractions * (1 + (dosePerFraction / alphaBeta));
    // Calculate EQD2
    const eqd2 = bed / ((2 / alphaBeta) + 1);
    // Display the result
    displayResult(`BED: ${bed.toFixed(2)} Gy, EQD2: ${eqd2.toFixed(2)} Gy`);
}

// Function to calculate BED and EQD2 using Python
function calculateWithPython() {
    // Destructure the form data
    const { dosePerFraction, totalFractions, alphaBeta } = getFormData();

    // Make a POST request to the '/calculate' endpoint
    fetch('/calculate', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        // Stringify the form data to send as the request body
        body: JSON.stringify({ dosePerFraction, totalFractions, alphaBeta }),
    })
    .then(response => response.json()) // Parse the response as JSON
    .then(data => {
        // Display the result
        displayResult(`BED: ${data.bed.toFixed(2)} Gy, EQD2: ${data.eqd2.toFixed(2)} Gy`);
    })
    .catch(error => console.error('Error:', error)); // Log any errors
}

// Function to display the result
function displayResult(result) {
    // Set the text of the 'result' element to the result
    document.getElementById('result').innerText = result;
}
